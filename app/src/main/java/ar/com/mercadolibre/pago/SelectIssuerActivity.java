package ar.com.mercadolibre.pago;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import ar.com.mercadolibre.network.Individuos;
import ar.com.mercadolibre.network.ServiceGenerator;
import ar.com.mercadolibre.network.models.CardIssuerResponse;
import ar.com.mercadolibre.pago.adapters.GeneralRecyclerAdapter;
import ar.com.mercadolibre.pago.decorations.SimpleDividerItemDecoration;
import ar.com.mercadolibre.pago.mappers.CardIssuerMapper;
import ar.com.mercadolibre.pago.models.CardIssuer;
import ar.com.mercadolibre.pago.models.CreditCardPayment;
import ar.com.mercadolibre.pago.models.MenuItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectIssuerActivity extends AppCompatActivity
        implements GeneralRecyclerAdapter.GeneralRecyclerAdapterListener<MenuItem<CardIssuer>> {

    private static final String TAG = "SelectIssuerAct";
    public static final int REQUEST_ID = 11;
    public static final String INDEX_RESULT_DATA = "issuer_selected";
    private CreditCardPayment creditCardPayment;

    private List<CreditCardPayment> creditCardPayments;
    private GeneralRecyclerAdapter<CardIssuer> cardIssuerGeneralRecyclerAdapter;

    private FrameLayout frameLayoutLoading;
    private RelativeLayout contentPanel;
    private RecyclerView recyclerView;

    private Callback<List<CardIssuerResponse>> issuersCallback
            = new Callback<List<CardIssuerResponse>>() {
        @Override
        public void onResponse(Call<List<CardIssuerResponse>> call,
                               Response<List<CardIssuerResponse>> response) {
            Log.d(TAG, "onResponse: Llegue");
            if (response.body().size() > 0) {
                cardIssuerGeneralRecyclerAdapter.addCreditCards(CardIssuerMapper.mapper(response.body()));
            } else {
                Toast.makeText(SelectIssuerActivity.this, "La tarjeta seleccionada no posee un issuer",
                        Toast.LENGTH_SHORT).show();
                SelectIssuerActivity.this.finish();
            }
            setLoadingMode(false);
        }

        @Override
        public void onFailure(Call<List<CardIssuerResponse>> call,
                              Throwable t) {
            Log.e(TAG, "onFailure: Error", t);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_issuer);


        frameLayoutLoading = (FrameLayout) findViewById(R.id.frameLayoutLoading);
        contentPanel = (RelativeLayout) findViewById(R.id.contentPanel);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        cardIssuerGeneralRecyclerAdapter = new GeneralRecyclerAdapter<>(this, this);
        recyclerView.setAdapter(cardIssuerGeneralRecyclerAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        setLoadingMode(true);

        this.creditCardPayment = getIntent()
                .getParcelableExtra(SelectCreditCardPaymentActivity.INDEX_RESULT_DATA);
        ServiceGenerator.createService(Individuos.class)
                .getCardIssuers(this.creditCardPayment.getId())
                .enqueue(issuersCallback);
    }

    void setLoadingMode(boolean loading) {
        frameLayoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        contentPanel.setVisibility(!loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemClick(MenuItem<CardIssuer> cardIssuer) {
        setResult(AppCompatActivity.RESULT_OK, new Intent()
                .putExtra(INDEX_RESULT_DATA, cardIssuer.getObject()));
        finish();
    }
}
