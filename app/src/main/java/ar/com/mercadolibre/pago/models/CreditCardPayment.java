package ar.com.mercadolibre.pago.models;

import android.os.Parcel;
import android.os.Parcelable;

public class CreditCardPayment implements Parcelable, MenuItem {
    private final String id;
    private final String name;
    private final String thumbnail;

    public CreditCardPayment(String id, String name, String thumbnail) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    protected CreditCardPayment(Parcel in) {
        id = in.readString();
        name = in.readString();
        thumbnail = in.readString();
    }

    public static final Creator<CreditCardPayment> CREATOR = new Creator<CreditCardPayment>() {
        @Override
        public CreditCardPayment createFromParcel(Parcel in) {
            return new CreditCardPayment(in);
        }

        @Override
        public CreditCardPayment[] newArray(int size) {
            return new CreditCardPayment[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public Object getObject() {
        return this;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public String toString() {
        return "CreditCardPayment{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(thumbnail);
    }
}
