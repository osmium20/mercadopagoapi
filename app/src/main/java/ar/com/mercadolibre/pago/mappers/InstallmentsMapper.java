package ar.com.mercadolibre.pago.mappers;

import java.util.ArrayList;
import java.util.List;

import ar.com.mercadolibre.network.models.PayerCost;
import ar.com.mercadolibre.pago.models.Installments;

/**
 * Created by joselarreal on 15/9/17.
 */

public class InstallmentsMapper {
    public static List<Installments> map(List<PayerCost> payerCosts) {
        List<Installments> installmentses = new ArrayList<>();
        for (PayerCost payerCost : payerCosts) {
            installmentses.add(new Installments(Integer.parseInt(payerCost.getInstallments()),
                    payerCost.getRecommendedMessage()));
        }
        return installmentses;
    }
}
