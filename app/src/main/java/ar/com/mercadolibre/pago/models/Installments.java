package ar.com.mercadolibre.pago.models;

public class Installments {

    private int numberInstallments;
    private String recommendMessage;

    public Installments(int numberInstallments, String recommendMessage) {
        this.numberInstallments = numberInstallments;
        this.recommendMessage = recommendMessage;
    }

    public String getRecommendMessage() {
        return recommendMessage;
    }

    public void setRecommendMessage(String recommendMessage) {
        this.recommendMessage = recommendMessage;
    }

    public int getNumberInstallments() {
        return numberInstallments;
    }

    public void setNumberInstallments(int numberInstallments) {
        this.numberInstallments = numberInstallments;
    }

    @Override
    public String toString() {
        return recommendMessage;
    }
}
