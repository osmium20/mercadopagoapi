package ar.com.mercadolibre.pago.models;


public class SessionPayment {
    // TODO: esto se deberia limpiar y formatear antes de mandar al servicio, segurodad
    private String amount;

    private CreditCardPayment creditCardPayment;
    private CardIssuer cardIssuer;
    private Installments installments;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public CreditCardPayment getCreditCardPayment() {
        return creditCardPayment;
    }

    public void setCreditCardPayment(CreditCardPayment creditCardPayment) {
        this.creditCardPayment = creditCardPayment;
    }

    public CardIssuer getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(CardIssuer cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    public Installments getInstallments() {
        return installments;
    }

    public void setInstallments(Installments installments) {
        this.installments = installments;
    }

    @Override
    public String toString() {
        return "SessionPayment{" +
                "amount='" + amount + '\'' +
                ", creditCardPayment=" + creditCardPayment +
                ", cardIssuer=" + cardIssuer +
                ", installments=" + installments +
                '}';
    }
}
