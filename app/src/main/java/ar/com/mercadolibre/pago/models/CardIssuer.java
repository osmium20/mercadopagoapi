package ar.com.mercadolibre.pago.models;


import android.os.Parcel;
import android.os.Parcelable;

public class CardIssuer implements Parcelable, MenuItem {
    private final String id;
    private final String name;
    private final String thumbnail;

    public CardIssuer(String id, String name, String thumbnail) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    protected CardIssuer(Parcel in) {
        id = in.readString();
        name = in.readString();
        thumbnail = in.readString();
    }

    public static final Creator<CardIssuer> CREATOR = new Creator<CardIssuer>() {
        @Override
        public CardIssuer createFromParcel(Parcel in) {
            return new CardIssuer(in);
        }

        @Override
        public CardIssuer[] newArray(int size) {
            return new CardIssuer[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public Object getObject() {
        return this;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public String toString() {
        return "CardIssuerResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(thumbnail);
    }
}
