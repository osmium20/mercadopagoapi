package ar.com.mercadolibre.pago;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ar.com.mercadolibre.network.Individuos;
import ar.com.mercadolibre.network.ServiceGenerator;
import ar.com.mercadolibre.network.models.InstallmentsResponse;
import ar.com.mercadolibre.network.models.PayerCost;
import ar.com.mercadolibre.pago.mappers.InstallmentsMapper;
import ar.com.mercadolibre.pago.models.CardIssuer;
import ar.com.mercadolibre.pago.models.CreditCardPayment;
import ar.com.mercadolibre.pago.models.Installments;
import ar.com.mercadolibre.pago.models.SessionPayment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private RelativeLayout relativeLayoutSelectCreditcard, relativeLayoutSelectIssuer;
    private FrameLayout frameLayoutLoading;
    private LinearLayout contentPanel, installmentsLinearLayout;
    private EditText amountEditText;
    private Spinner installmentsSpinner;
    private ImageView creditCardSelectedIcon, issuerSelectedIcon;
    private TextView creditCardSelectedName, issuerSelectedName;
    private Button buttonSubmit;

    private SessionPayment sessionPayment;
    private List<Installments> installmentses = new ArrayList<>();
    private ArrayAdapter<Installments> spinnerArrayAdapter;

    Callback<List<InstallmentsResponse>> installmentsResponseCallback
            = new Callback<List<InstallmentsResponse>>() {
        @Override
        public void onResponse(Call<List<InstallmentsResponse>> call,
                               Response<List<InstallmentsResponse>> response) {
            List<InstallmentsResponse> installmentsResponse = response.body();
            //segun revise, siempre se deberia tomar el primero, revisar esto si se necesita a futuro
            try {
                List<PayerCost> payerCosts = installmentsResponse.get(0).getPayerCosts();
                installmentses = InstallmentsMapper.map(payerCosts);
                spinnerArrayAdapter.clear();
                spinnerArrayAdapter.addAll(installmentses);
                installmentsLinearLayout.setVisibility(View.VISIBLE);
            } catch (NullPointerException ex) {
                Toast.makeText(MainActivity.this,
                        "Ops! Es posible que el importe sea muy elevado para la transaccion.",
                        Toast.LENGTH_SHORT).show();
            }
            setLoading(false);
        }

        @Override
        public void onFailure(Call<List<InstallmentsResponse>> call, Throwable t) {
            Toast.makeText(MainActivity.this, "Ops! Selecciona otro banco!",
                    Toast.LENGTH_SHORT).show();
            setLoading(false);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        amountEditText = (EditText) findViewById(R.id.amountEditText);
        relativeLayoutSelectCreditcard = (RelativeLayout)
                findViewById(R.id.relativeLayoutSelectCreditcard);
        relativeLayoutSelectIssuer = (RelativeLayout)
                findViewById(R.id.relativeLayoutSelectIssuer);
        frameLayoutLoading = (FrameLayout) findViewById(R.id.frameLayoutLoading);
        contentPanel = (LinearLayout) findViewById(R.id.contentPanel);
        installmentsLinearLayout = (LinearLayout) findViewById(R.id.installmentsLinearLayout);

        creditCardSelectedName = (TextView) findViewById(R.id.creditCardSelectedName);
        issuerSelectedName = (TextView) findViewById(R.id.issuerSelectedName);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);

        installmentsSpinner = (Spinner) findViewById(R.id.installmentsSpinner);

        creditCardSelectedIcon = (ImageView) findViewById(R.id.creditCardSelectedIcon);
        issuerSelectedIcon = (ImageView) findViewById(R.id.issuerSelectedIcon);


        spinnerArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, installmentses);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        installmentsSpinner.setAdapter(spinnerArrayAdapter);

        if (sessionPayment == null) {
            sessionPayment = new SessionPayment();
        }

        relativeLayoutSelectCreditcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validacion muy basica de que algo esté escrito
                if (checkAmount()) {
                    startActivityForResult(new Intent(MainActivity.this,
                                    SelectCreditCardPaymentActivity.class),
                            SelectCreditCardPaymentActivity.REQUEST_ID);
                    amountEditText.setEnabled(false);
                }
            }
        });

        relativeLayoutSelectIssuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkAmount()) {
                    Intent intent = new Intent(MainActivity.this,
                            SelectIssuerActivity.class);

                    intent.putExtra(SelectCreditCardPaymentActivity.INDEX_RESULT_DATA,
                            sessionPayment.getCreditCardPayment());

                    startActivityForResult(intent,
                            SelectIssuerActivity.REQUEST_ID);
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                String messageSelected = String.format(
                        "Monto: %s\n" +
                                "Credit Card: %s\n" +
                                "Issuer: %s\n" +
                                "Installments: %s",
                        sessionPayment.getAmount(),
                        sessionPayment.getCreditCardPayment().getName(),
                        sessionPayment.getCardIssuer().getName(),
                        installmentsSpinner.getSelectedItem().toString());
                builder.setMessage(messageSelected)
                        .setTitle("Pago a enviar")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                MainActivity.this.resetForm();
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
            }
        });

        amountEditText.requestFocus();
    }

    private boolean checkAmount() {
        if (amountEditText.getText().toString().length() > 0) {
            return true;
        } else {
            amountEditText.setError(getString(R.string.msg_validation_amount));
            return false;
        }
    }

    private void refreshCreditCard() {
        if (sessionPayment.getCreditCardPayment() != null) {
            creditCardSelectedName.setText(sessionPayment.getCreditCardPayment().getName());
            Picasso.with(this).load(sessionPayment.getCreditCardPayment().getThumbnail())
                    .into(creditCardSelectedIcon);
            creditCardSelectedIcon.setVisibility(View.VISIBLE);
            relativeLayoutSelectIssuer.setVisibility(View.VISIBLE);
            sessionPayment.setAmount(amountEditText.getText().toString());
        } else {
            creditCardSelectedName.setText(R.string.textiview_selecciona_una_tarjeta);
            creditCardSelectedIcon.setVisibility(GONE);
            relativeLayoutSelectIssuer.setVisibility(GONE);
            sessionPayment.setCardIssuer(null);
            sessionPayment.setAmount(null);
        }
    }

    private void refreshIssuer() {
        if (sessionPayment.getCardIssuer() != null) {
            issuerSelectedName.setText(sessionPayment.getCardIssuer().getName());
            Picasso.with(this).load(sessionPayment.getCardIssuer().getThumbnail())
                    .into(issuerSelectedIcon);
            issuerSelectedIcon.setVisibility(View.VISIBLE);
            relativeLayoutSelectIssuer.setVisibility(View.VISIBLE);
            setLoading(true);
            ServiceGenerator.createService(Individuos.class).getInstallments(
                    sessionPayment.getCreditCardPayment().getId(),
                    sessionPayment.getCardIssuer().getId(), sessionPayment.getAmount())
                    .enqueue(installmentsResponseCallback);
        } else {
            // TODO: 15/9/17 agregar algo en este else
        }
    }

    private void resetForm() {
        sessionPayment = new SessionPayment();//reset session

        amountEditText.setEnabled(true);
        amountEditText.setText("");

        issuerSelectedIcon.setVisibility(View.GONE);
        creditCardSelectedIcon.setVisibility(View.GONE);

        relativeLayoutSelectIssuer.setVisibility(View.GONE);
        installmentsLinearLayout.setVisibility(View.GONE);

        creditCardSelectedName.setText(R.string.textiview_selecciona_una_tarjeta);
        issuerSelectedName.setText(R.string.textiview_selecciona_un_banco);
    }

    private void setLoading(boolean loading) {
        frameLayoutLoading.setVisibility(loading ? View.VISIBLE : GONE);
        contentPanel.setVisibility(!loading ? View.VISIBLE : GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SelectCreditCardPaymentActivity.REQUEST_ID &&
                resultCode == AppCompatActivity.RESULT_OK) {
            sessionPayment.setCreditCardPayment(
                    (CreditCardPayment) data.getParcelableExtra(
                            SelectCreditCardPaymentActivity.INDEX_RESULT_DATA));
            refreshCreditCard();
        }

        if (requestCode == SelectIssuerActivity.REQUEST_ID &&
                resultCode == AppCompatActivity.RESULT_OK) {
            sessionPayment.setCardIssuer((CardIssuer) data.getParcelableExtra(
                    SelectIssuerActivity.INDEX_RESULT_DATA));
            refreshIssuer();
        }
    }

}
