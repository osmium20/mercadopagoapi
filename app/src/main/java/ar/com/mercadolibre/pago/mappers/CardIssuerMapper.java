package ar.com.mercadolibre.pago.mappers;

import java.util.ArrayList;
import java.util.List;

import ar.com.mercadolibre.network.models.CardIssuerResponse;
import ar.com.mercadolibre.pago.models.CardIssuer;

/**
 * Created by joselarreal on 15/9/17.
 */

public class CardIssuerMapper {

    public static List<CardIssuer> mapper(List<CardIssuerResponse> cardIssuers) {
        List<CardIssuer> cardIssuerList = new ArrayList<>();
        for (CardIssuerResponse cardIssuer : cardIssuers) {
            cardIssuerList.add(new CardIssuer(cardIssuer.getId(), cardIssuer.getName(),
                    cardIssuer.getThumbnail()));
        }
        return cardIssuerList;
    }
}
