package ar.com.mercadolibre.pago;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import java.util.List;

import ar.com.mercadolibre.network.Individuos;
import ar.com.mercadolibre.network.ServiceGenerator;
import ar.com.mercadolibre.network.models.PaymentMethodResponse;
import ar.com.mercadolibre.pago.adapters.GeneralRecyclerAdapter;
import ar.com.mercadolibre.pago.decorations.SimpleDividerItemDecoration;
import ar.com.mercadolibre.pago.mappers.PaymentMapper;
import ar.com.mercadolibre.pago.models.CreditCardPayment;
import ar.com.mercadolibre.pago.models.MenuItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectCreditCardPaymentActivity extends AppCompatActivity
        implements GeneralRecyclerAdapter.GeneralRecyclerAdapterListener<MenuItem<CreditCardPayment>> {
    private List<CreditCardPayment> creditCardPayments;
    private GeneralRecyclerAdapter<CreditCardPayment> creditCardAdapter;

    private static final String TAG = "SelectCreditCard";
    public static final String INDEX_RESULT_DATA = "card_selected";

    public static final int REQUEST_ID = 10;

    private FrameLayout frameLayoutLoading;
    private RelativeLayout contentPanel;
    private RecyclerView recyclerView;


    private Callback<List<PaymentMethodResponse>> paymantsCallback =
            new Callback<List<PaymentMethodResponse>>() {
                @Override
                public void onResponse(Call<List<PaymentMethodResponse>> call,
                                       Response<List<PaymentMethodResponse>> response) {
                    creditCardPayments = PaymentMapper.getCreditCardFromPayments(response.body());
                    creditCardAdapter.addCreditCards(creditCardPayments);
                    setLoadingMode(false);
                }

                @Override
                public void onFailure(Call<List<PaymentMethodResponse>> call, Throwable t) {
                    Log.e(TAG, "onFailure: Llegue", t);
                    setLoadingMode(false);
                }

            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_credit_card_payment);
        frameLayoutLoading = (FrameLayout) findViewById(R.id.frameLayoutLoading);
        contentPanel = (RelativeLayout) findViewById(R.id.contentPanel);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        creditCardAdapter = new GeneralRecyclerAdapter<>(this, this);
        recyclerView.setAdapter(creditCardAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        setLoadingMode(true);
        ServiceGenerator.createService(Individuos.class)
                .getPaymentMethods()
                .enqueue(paymantsCallback);
    }

    void setLoadingMode(boolean loading) {
        frameLayoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        contentPanel.setVisibility(!loading ? View.VISIBLE : View.GONE);
    }


    @Override
    public void onItemClick(MenuItem<CreditCardPayment> creditCardPayment) {
        setResult(AppCompatActivity.RESULT_OK, new Intent().putExtra(INDEX_RESULT_DATA,
                creditCardPayment.getObject()));
        finish();
    }
}
