package ar.com.mercadolibre.pago.mappers;


import java.util.ArrayList;
import java.util.List;

import ar.com.mercadolibre.network.models.PaymentMethodResponse;
import ar.com.mercadolibre.pago.models.CreditCardPayment;

/**
 * Created by joselarreal on 15/9/17.
 */
public class PaymentMapper {

    public static List<CreditCardPayment> getCreditCardFromPayments(
            List<PaymentMethodResponse> paymentMethods) {
        List<CreditCardPayment> creditCardPayments = new ArrayList<>();
        String typeCard = "credit_card";
        for (PaymentMethodResponse paymentMethod : paymentMethods) {
            if (paymentMethod.getPaymentTypeId().equals(typeCard))
                creditCardPayments.add(new CreditCardPayment(paymentMethod.getId(),
                        paymentMethod.getName(), paymentMethod.getThumbnail()));
        }
        return creditCardPayments;
    }
}
