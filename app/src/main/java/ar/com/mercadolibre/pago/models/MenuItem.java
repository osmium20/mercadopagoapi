package ar.com.mercadolibre.pago.models;


public interface MenuItem<T> {
    String getThumbnail();

    String getName();

    T getObject();
}
