package ar.com.mercadolibre.pago.adapters;

/**
 * Created by joselarreal on 15/9/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ar.com.mercadolibre.pago.R;
import ar.com.mercadolibre.pago.models.MenuItem;


public class GeneralRecyclerAdapter<T extends MenuItem>
        extends RecyclerView.Adapter<GeneralRecyclerAdapter.ViewHolder> {
    private List<T> itemsList;
    private Context context;
    private GeneralRecyclerAdapterListener mListenter;

    public GeneralRecyclerAdapter(Context context,
                                  GeneralRecyclerAdapterListener creditCardListener) {
        itemsList = new ArrayList<>();
        this.context = context;
        this.mListenter = creditCardListener;
    }

    @Override
    public GeneralRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GeneralRecyclerAdapter.ViewHolder holder, int position) {
        MenuItem creditCardPayment = itemsList.get(position);
        holder.menuItem = creditCardPayment;
        holder.name.setText(creditCardPayment.getName());
        Picasso.with(context).load(creditCardPayment.getThumbnail()).into(holder.icon);

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void addCreditCards(List<T> itemsList) {
        this.itemsList.addAll(itemsList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        MenuItem menuItem;
        TextView name;
        ImageView icon;

        ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListenter.onItemClick(menuItem);
                }
            });
        }
    }

    public interface GeneralRecyclerAdapterListener<T> {
        void onItemClick(T t);
    }
}
