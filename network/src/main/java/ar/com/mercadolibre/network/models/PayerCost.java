package ar.com.mercadolibre.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joselarreal on 15/9/17.
 */

public class PayerCost {
    private String installments;

    @SerializedName("recommended_message")
    private String recommendedMessage;

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    public String getInstallments() {
        return installments;
    }

    public void setInstallments(String installments) {
        this.installments = installments;
    }
}
