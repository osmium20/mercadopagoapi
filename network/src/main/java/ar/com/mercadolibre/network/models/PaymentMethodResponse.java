package ar.com.mercadolibre.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joselarreal on 15/9/17.
 */
public class PaymentMethodResponse {

    private String id;
    private String name;

    @SerializedName("payment_type_id")
    private String paymentTypeId;

    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "PaymentMethodResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
