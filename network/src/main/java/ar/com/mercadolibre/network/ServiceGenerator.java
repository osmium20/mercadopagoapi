package ar.com.mercadolibre.network;


import ar.com.mercadolibre.network.interceptor.ApiInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by joselarreal on 15/9/17.
 */
public class ServiceGenerator {

    protected static final String BASE_SERVICE = "https://api.mercadopago.com/v1/";
    protected static final String API_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";

    private static HttpLoggingInterceptor httpLoggingInterceptor =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static ApiInterceptor apiInterceptor = new ApiInterceptor(API_KEY);


    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_SERVICE)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        if (!httpClient.interceptors().contains(httpLoggingInterceptor)) {
            httpClient.addInterceptor(httpLoggingInterceptor);
        }
        if (!httpClient.interceptors().contains(apiInterceptor)) {
            httpClient.addInterceptor(apiInterceptor);
        }

        builder.client(httpClient.build());
        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }
}


