package ar.com.mercadolibre.network;


import java.util.List;

import ar.com.mercadolibre.network.models.CardIssuerResponse;
import ar.com.mercadolibre.network.models.InstallmentsResponse;
import ar.com.mercadolibre.network.models.PaymentMethodResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by joselarreal on 15/9/17.
 */

public interface Individuos {

    @GET("payment_methods")
    Call<List<PaymentMethodResponse>> getPaymentMethods();

    @GET("payment_methods/card_issuers")
    Call<List<CardIssuerResponse>> getCardIssuers(@Query("payment_method_id") String cardId);

    @GET("payment_methods/installments")
    Call<List<InstallmentsResponse>> getInstallments(@Query("payment_method_id") String cardId,
                                                     @Query("issuer.id") String issuerId,
                                                     @Query("amount") String amount);

}
