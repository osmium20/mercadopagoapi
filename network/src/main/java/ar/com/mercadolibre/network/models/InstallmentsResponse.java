package ar.com.mercadolibre.network.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by joselarreal on 15/9/17.
 */

public class InstallmentsResponse {
    @SerializedName("payment_method_id")
    private String paymentMethodId;

    @SerializedName("payer_costs")
    private List<PayerCost> payerCosts;

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }
}
